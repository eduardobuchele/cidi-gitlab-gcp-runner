Preparação do Ambiente:
- Criar uma instancia no Google
	- https://console.cloud.google.com/
	
-   Criar as chaves SSH
	- https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
		- putty.exe
		- puttygen.exe   (Gerar a key)
		- Editar instancia e salvar SSH
		- Abrir o putty.exe
			- Salvar o host com IP externo da instancia criada
			- EM SSH | Auth Adicionar a SSH private key salva

- Instalar docker na instancia
	- https://docs.docker.com/engine/install/debian/
		-  curl -fsSL https://get.docker.com -o get-docker.sh
		-  sudo sh ./get-docker.sh --dry-run

- Atualizar a instancia
	- apt update 
- Instalar o gitlab-runner
	- https://docs.gitlab.com/runner/install/linux-repository.html
		- curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
		- sudo apt-get install gitlab-runner
		- gitlab-runner register
			-  utilizar dados do projeto no gitlab > Settings > CI/CD > Project Runner
			- Enter a description for the runner: gcp
			- Enter tags for the runner (comma-separated): gcp
			- Enter an executor: docker, docker-windows, parallels, shell, ssh, virtualbox, docker-autoscaler, custom, docker-ssh+machine, docker+machine, instance, kubernetes, docker-ssh:
				- shell       
		- gitlab-runner start
    
- Já deve aparecer o runner em: gitlab > Settings > CI/CD > Project Runner

- Na instancia 
	- Dar permissão para i gitlab runner usar o docker
		cat /etc/passwd
		sudo usermod -aG docker gcp
		sudo usermod -aG docker gitlab-runner
		sudo su
		su gitlab-runner
		docker login
		exit
		
